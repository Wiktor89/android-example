package com.example.app;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static android.util.Log.d;

public class MainActivity extends AppCompatActivity {
    private RequestQueue queue;
    private TextView responseServer;
    private TextView requestServer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        queue = Volley.newRequestQueue(this);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        Button button = findViewById(R.id.b_request_to_server);
        responseServer = findViewById(R.id.respone_server);
        requestServer = findViewById(R.id.request_server);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                jsonParse();
            }
        });


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView text = findViewById(R.id.login);
                TextView password = findViewById(R.id.password);
                d("Test {}", text.getText().toString().concat(";\n password = ").concat(password.getText().toString()));
                sendPost(text, password);
            }
        });
    }

    private void jsonParse() {
        TextView login = findViewById(R.id.login);
        TextView password = findViewById(R.id.password);
        User user = new User(login.getText().toString(), password.getText().toString());
        TextView urlTextField = findViewById(R.id.url_request);
        String url = "http://".concat(urlTextField.getText().toString());
        JSONObject json = new JSONObject();
        try {
            json.put("login", user.getLogin());
            json.put("password", user.getPassword());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, url, json, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Object login = response.get("login");
                            Object password = response.get("password");
                            responseServer.setText("Login: " + login.toString() + "\npassword: " + password.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();

                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("Authorization", "Basic cm9vdDpyb290");
                params.put("Accept", "application/json");
                return params;
            }
        };
        requestServer.setText(jsonObjectRequest.getUrl());
        queue.add(jsonObjectRequest);
    }

    public void sendPost(final TextView text, TextView password) {
        Log.d("asdsadad", "sadsad");
        String url = "http://192.168.0.145:8080/api/v1/";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
//                        text.setText("Response is: "+ response.substring(0,500));
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        queue.add(stringRequest);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
